<?php
	include("functions_inc.php");
	include("validate_juegos.php");
	session_start();

	$error = false;
	$error_nom = false;
	if (isset($_POST['alta'])) {

		$error_nom = EsRequerido($_POST['nombre']);
		if($error_nom){
			$error_nom = "El nombre del juego no puede estar en blanco";
			$error = true;
		}
		if (!$error) {
			//debug($_POST);
			//exit();
			//print_r($_POST);
			$mensaje="Su registro se ha efectuado correctamente";
			$_SESSION['user']=$_POST;
			$_SESSION['msje']=$mensaje;
			$callback = 'result_juegos.php';

			//header("Location:$callback");
			die('<script>top.location.href="'.$callback .'";</script>');
		}
	}
	include 'formulario_juegos.php';
