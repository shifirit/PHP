<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//ES" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Formulario con un botón especial</title>
	<link rel="stylesheet" href="estilos.css" type="text/css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.js"></script>
	<script src="validate_juego.js"></script>
	<script type="text/javascript">

  $(function() {
    $('#demo1').datepicker({
      dateFormat: 'dd/mm/yy',
      changeMonth: true,
      changeYear: true,
      minDate: new Date(1900,1-1,1),maxDate:'-18Y',
			defaultDate:new Date(1970,1-1,1),
			yearRange:'-90:-18'
    //  onSelect: function(selectedDate) {
        //alert(selectedDate);
      //}
    });
	});
	</script>

</head>

<body>

  <h1> Registro de venta de videojuego</h1>
  <div class="formulario">
	<form name="form" class="contacto" id="formusers" method="post" onsubmit="return validate_juego();" action="index.php">

		<div>
			<p>
				<label for="nombre">Nombre del videojuego:</label>
				<input name="nombre" id="nombre" type="text" placeholder="Nombre del videojuego" value="" />
				<span id="e_nombre" class="styerror"></span>
				<?php
				if ($error_nom != "")
							print ("<BR><SPAN CLASS='styerror' color: #ff0000;>" . "* ".$error_nom . "</SPAN>");
				?>
	</p>



		</div>

		<div><label>Pais de venta:</label><input id="pais" type='text' name="pais" placeholder="Pais de venta" class="paisPais de venta" value="" ?>
		<span id="e_pais" class="styerror"></span></div>

		<div><label>Empresa:</label><input id="empresa" type='text' name="empresa" placeholder="Empresa de creacion del juego" class="empresa" value="" ?>
		<span id="e_empresa" class="styerror"></span></div>

		<div><label>Tu Email:</label><input id="email" type='text' name="email" placeholder="Escribe tu email" class="email" value="" ?>
		<span id="e_email" class="styerror"></span></div>

		<div><label>Estado del videojuego:</label><input id="estado" type='text' name="estado" placeholder="Estado del videojuego" class="estado" value="" ?>
		<span id="e_estado" class="styerror"></span></div>

		<p><label>Fecha de lanzamiento del juego:</label><input id="demo1" type="text" name="datepiker" class="datepiker" placeholder="Fecha de lanzamiento del juego" value="" readonly="readonly" ?></p>

		<div><label>Breve observación:</label><textarea id="observacion" rows='6' type='text' name="observacion" placeholder="Breve descripcion o comentario sobre el juego" class="estado" value="" ?> </textarea>
		<span id="e_observacion" class="styerror"></span></div>


		<div>Plataforma disponible:
					<br>XBOX 360 <input type="checkbox" name="consola[]" value="xbox 360" >
							PS4	<input type="checkbox" name="consola[]" value="ps4">
							Nintendo Switch <input type="checkbox" name="consola[]" value="Nintendo Switch" >
							Wii U <input type="checkbox" name="consola[]" value="Wii U" >
							PSP <input type="checkbox" name="consola[]" value="psp" >
							Nintendo DS <input type="checkbox" name="consola[]" value="DS" >
							Pc <input type="checkbox" name="consola[]" value="Pc" >
							Otras <input type="checkbox" name="consola[]" value="Otros"></br>
		</div>
		<div>Forma de Pago:
					<br>Paypal <input type="checkbox" name="pago[]" value="paypal" >
					VISA <input type="checkbox" name="pago[]" value="visa">
					Transferencia bancaria <input type="checkbox" name="pago[]" value="transferencia" >
					Otros <input type="checkbox" name="pago[]" value="Otros"></br>
		</div>

		<div>
			<br>
      	<td>Género </td>
      	<td><select name="genero" id="genero" class="genero">
        	<option value="Accion">Acción</option>
        	<option value="Disparos">Disparos</option>
        	<option value="Estrategia">Estrategia</option>
        	<option value="Simulacion">Simulación</option>
					<option value="Deporte">Deporte</option>
					<option value="Carreras">Carreras</option>
					<option value="Aventura">Aventura</option>
					<option value="Rol">Rol</option>
					<option value="Otros">Otros</option>
      	</select>
				</td>
			</br>
    </div>
    <div>
			<br><td>Edad recomendad del videojuegos:</td>
			<td>
				<br>
       	Todos los publicos <input name="edad" type="radio" value="tp" checked="checked">
        +3 <input name="edad" type="radio" value="+3">
				+12 <input name="edad" type="radio" value="+12">
        +18<input name="edad" type="radio" value="+18"></td>
			</br>
    </br>
	</div>



		<div class="demo"><input name="alta" type="submit" value="Registrar"></div>
	</form>
	</div>

</body>
</html>
