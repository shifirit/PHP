<?php
	function validate_juegos(){
		$error='';
		$filtro = array(
			/*'usuario' => array(
				'filter'=>FILTER_VALIDATE_REGEXP,
				'options'=>array('regexp'=>'/^.{4,20}$/')
			),
			*/
			'nombre' => array(
				'filter'=>FILTER_VALIDATE_REGEXP,
				'options'=>array('regexp'=>'/^\D{3,30}$/')
			),

			'pais' => array(
				'filter'=>FILTER_VALIDATE_REGEXP,
				'options'=>array('regexp'=>'/^[a-zA-Z0-9]*$/')
			),

			'empresa' => array(
				'filter'=>FILTER_VALIDATE_REGEXP,
				'options'=>array('regexp'=>'/^[a-zA-Z0-9]*$/')
			),


			'email' => array(
				'filter'=>FILTER_CALLBACK,
				'options'=>'validatemail'
			),

			'estado' => array(
				'filter'=>FILTER_VALIDATE_REGEXP,
				'options'=>array('regexp'=>'/^[a-zA-Z0-9]*$/')
			),

			'observacion' => array(
				'filter'=>FILTER_VALIDATE_REGEXP,
				'options'=>array('regexp'=>'/^[a-zA-Z0-9]*$/')
			),


			'date_lanzamiento' => array(
				'filter' => FILTER_VALIDATE_REGEXP,
				'options' => array('regexp' => '/\d{2}.\d{2}.\d{4}$/')
			)

		);


		$resultado=filter_input_array(INPUT_POST,$filtro);
		if(!$resultado['nombre']){
			$error='Nombre debe tener de 3 a 30 caracteres';

		}elseif(!$resultado['pais']){
			$error='El pais debe tener de 4 a 120 caracteres';

		}elseif(!$resultado['empresa']){
			$error='La empresa debe tener de 4 a 120 caracteres';

		}elseif(!$resultado['email']){
			$error='El email debe contener de 5 a 50 caracteres y debe ser un email valido';

		}elseif(!$resultado['estado']){
			$error='El estado debe tener de 4 a 120 caracteres';

		}elseif(!$resultado['observacion']){
			$error='La observacion debe tener de 3 a 120 caracteres';


		}elseif(!$resultado['date_lanzamiento']){
			$error='Formato fecha dd/mm/yy';


		}else{
			 return $return=array('resultado'=>true,'error'=>$error,'datos'=>$resultado);
		};
		return $return=array('resultado'=>false , 'error'=>$error,'datos'=>$resultado);
	};

	function validatemail($email){
			$email = filter_var($email, FILTER_SANITIZE_EMAIL);
			if(filter_var($email, FILTER_VALIDATE_EMAIL)){
				if(filter_var($email, FILTER_VALIDATE_REGEXP, array('options' => array('regexp'=> '/^.{5,50}$/')))){
					return $email;
				}
			}
			return false;
	}

	function debug($array){
		echo "<pre>";
		print_r($array);
		echo "</pre><br>";
	}
?>
