-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-06-2016 a las 08:19:43
-- Versión del servidor: 5.5.49-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `DB`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `registro` (
  `nombre` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `pais` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `empresa` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `estado` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `observacion` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `fecha` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `consola` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `pago` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `genero` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `edad` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;


CREATE DATABASE videojuegos;
use videojuegos;
CREATE TABLE registro(
    id int AUTO_INCREMENT not null PRIMARY KEY,
    nombre varchar(255),
    pais varchar(255),
    empresa varchar(255),
    email varchar(255),
    estado varchar(255),
    observacion varchar(255),
    fecha varchar(255),
    consola varchar(255),
    pago varchar(255),
    genero varchar(255),
    edad varchar(255)
);

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO registro (nombre, pais,empresa,email,estado,observacion,fecha, consola, pago, genero,edad) VALUES
('World of Warcraft', 'España', 'Blizzard', 'shifirit@gmail.com', 'Bueno','Vive una aventura magica', '19/04/1993', 'XBOX:PS4', 'PayPal:', 'Aventura:', '+3'),
('League of Legends', 'España', 'Riot', 'juan@gmail.com', 'Malo','Mire usted señor', '19/07/2000', 'XBOX:PS4', 'PayPal:', 'Rol:', 'tp'),
('Cs GO', 'España', 'Valvet', 'pedro@gmail.com', 'Ok','El juego es entretenido', '19/07/1800', 'XBOX:', 'PayPal:', 'Accion:', '+12'),






/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
