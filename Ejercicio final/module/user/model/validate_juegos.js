//alert("Hola");

function validate_nombre(nombre) {
    if (nombre.length > 0) {
        var regexp ="|^[a-zA-Z]+(\s*[a-zA-Z]*)*[a-zA-Z]+$|";
        return regexp.test(nombre);
    }
    return false;
}

function validate_pais(pais) {
    if (pais.length > 0) {
        var regexp ="|^[a-zA-Z]+(\s*[a-zA-Z]*)*[a-zA-Z]+$|";
        return regexp.test(pais);
    }
    return false;
}
function validate_empresa(empresa) {
    if (empresa.length > 0) {
          var regexp ="|^[a-zA-Z]+(\s*[a-zA-Z]*)*[a-zA-Z]+$|";
        return regexp.test(empresa);
    }
    return false;
}

function validate_email(email) {
    if (email.length > 0) {
        var regexp = /^[A-Z0-9._%+-]+@(?:[A-Z0-9-]+.)+[A-Z]{2,4}$/i;
        return regexp.test(email);
    }
    return false;
}

function validate_estado(estado) {
    if (estado.length > 0) {
        var regexp ="|^[a-zA-Z]+(\s*[a-zA-Z]*)*[a-zA-Z]+$|";
        return regexp.test(estado);
    }
    return false;
}
function validate_observacion(observacion) {
    if (observacion.length > 0) {
          var regexp ="|^[a-zA-Z]+(\s*[a-zA-Z]*)*[a-zA-Z]+$|";
        return regexp.test(observacion);
    }
    return false;
}



function validate_edad(texto){
    var i;
    var ok=0;
    for(i=0; i<texto.length;i++){
        if(texto[i].checked){
            ok=1
        }
    }

    if(ok==1){
        return true;
    }
    if(ok==0){
        return false;
    }
}

function validate_fecha(texto){
    if (texto.length > 0){
        return true;
    }
    return false;
}


function validate_pais(texto){
    if (texto.length > 0){
        return true;
    }
    return false;
}

function validate_genero(array){
    var result=false;
    for ( var i = 0, l = array.options.length, o; i < l; i++ ){
        o = array.options[i];
        if ( o.selected ){
            result= true;
        }
    }
    return result;
}


function validate_consola(array){
    var i;
    var ok=0;
    for(i=0; i<array.length;i++){
        if(array[i].checked){
            ok=1
        }
    }

    if(ok==1){
        return true;
    }
    if(ok==0){
        return false;
    }
}
function validate_pago(array){
    var i;
    var ok=0;
    for(i=0; i<array.length;i++){
        if(array[i].checked){
            ok=1
        }
    }

    if(ok==1){
        return true;
    }
    if(ok==0){
        return false;
    }
}


function validate_game(){
  //alert("HOLA")

    var result=true;

    var nombre = document.getElementById('nombre').value;
  //  alert(nombre);
  	var pais = document.getElementById('pais').value;
  //  alert(pais);
    var empresa = document.getElementById('empresa').value;
    //alert(empresa);
  	var email = document.getElementById('email').value;
  //  alert(email);
    var estado = document.getElementById('estado').value;
  //  alert(estado);
  	var observacion = document.getElementById('observacion').value;
  //  alert(observacion);
    var date_lanzamiento=document.getElementById('date_lanzamiento').value;


    var v_nombre = validate_nombre(nombre);
  	var v_pais = validate_pais(pais);
    var v_empresa = validate_empresa(empresa);
  	var v_email = validate_email(email);
    var v_estado = validate_estado(estado);
  	var v_observacion = validate_observacion(observacion);
    var v_date_lanzamiento= validate_fecha(date_lanzamiento);

  if (!v_nombre) {
        document.getElementById('e_nombre').innerHTML = "El nombre introducido no es valido js";
        result = false;
    } else {
        document.getElementById('e_nombre').innerHTML = "";
    }

  if (!v_pais) {
        document.getElementById('e_pais').innerHTML = "El pais introducido no es valido js";
        result = false;
    } else {
        document.getElementById('e_pais').innerHTML = "";
    }

    if (!v_empresa) {
          document.getElementById('e_empresa').innerHTML = "La empresa introducidad no es valida js";
          result = false;
      } else {
          document.getElementById('e_empresa').innerHTML = "";
      }

  if (!v_email) {
        document.getElementById('e_email').innerHTML = "El email introducido no es valido js";
        result = false;
    } else {
        document.getElementById('e_email').innerHTML = "";
    }
    if (!v_estado) {
        document.getElementById('e_estado').innerHTML = "El estado no es valido js";
        result = false;
    } else {
        document.getElementById('e_estado').innerHTML = "";
    }

  if (!v_observacion) {
        document.getElementById('e_observacion').innerHTML = "Observacion no valida revisala js";
        result = false;
    } else {
        document.getElementById('e_observacion').innerHTML = "";
    }

    if(!v_date_lanzamiento){
        document.getElementById('e_date_lanzamiento').innerHTML = " * No has introducido ninguna fecha js";
        result=false;
    }else{
        document.getElementById('e_date_lanzamiento').innerHTML = "";
    }

/*
    if(!v_consola){
        document.getElementById('e_consola').innerHTML = "  No has seleccionado ninguna consola EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEO";
        check=false;
    }else{
        document.getElementById('e_consola').innerHTML = "";
    }

    if(!v_pago){
        document.getElementById('e_pago').innerHTML = " * No has seleccionado ninguna forma de pago EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEO";
        check=false;
    }else{
        document.getElementById('e_pago').innerHTML = "";
    }

    if(!v_genero){
        document.getElementById('e_genero').innerHTML = " * No has seleccionado ningun genero EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEO";
        check=false;
    }else{
        document.getElementById('e_genero').innerHTML = "";
    }

    if(!v_edad){
        document.getElementById('e_edad').innerHTML = " * No has seleccionado ninguna edad EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEO";
        check=false;
    }else{
        document.getElementById('e_edad').innerHTML = "";
    }
    */


    return result;
}
