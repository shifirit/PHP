<?php

    function validate_nombre($texto){
        //$reg="/^[a-zA-Z]*$/";
        $reg="|^[a-zA-Z]+(\s*[a-zA-Z]*)*[a-zA-Z]+$|";
        return preg_match($reg,$texto);
    }

    function validate_pais($texto){
      //  $reg="/^[a-zA-Z]*$/";
        $reg="|^[a-zA-Z]+(\s*[a-zA-Z]*)*[a-zA-Z]+$|";
        return preg_match($reg,$texto);
    }

    function validate_empresa($texto){
      //  $reg="/^[a-zA-Z]*$/";
      $reg="|^[a-zA-Z]+(\s*[a-zA-Z]*)*[a-zA-Z]+$|";
        return preg_match($reg,$texto);
    }

  function validate_email($texto) {
          $reg ="/^[A-Z0-9._%+-]+@(?:[A-Z0-9-]+.)+[A-Z]{2,4}$/i ";
          return preg_match($reg,$texto);
    }


    function validate_estado($texto){
      //  $reg="/^[a-zA-Z]*$/";
        $reg="|^[a-zA-Z]+(\s*[a-zA-Z]*)*[a-zA-Z]+$|";
        return preg_match($reg,$texto);
    }


    function validate_edad($texto){
        if(!isset($texto) || empty($texto)){
            return false;
        }else{
            return true;
        }
    }

    function validate_fecha($texto){
        if(empty($texto)){
            return false;
        }else{
            return true;
        }
    }


    function validate_genero($texto){
        if(!isset($texto) || empty($texto)){
            return false;
        }else{
            return true;
        }
    }

    function validate_observacion($texto){
        if(empty($texto)){
            return false;
        }else{
            return true;
        }
    }

    function validate_consola($texto){
        if(!isset($texto) || empty($texto)){
            return false;
        }else{
            return true;
        }
    }

    function validate_pago($texto){
        if(!isset($texto) || empty($texto)){
            return false;
        }else{
            return true;
        }
    }

    function validate(){
      
        $check=true;

        $error=array();

        $nombre=$_POST['nombre'];

        $pais=$_POST['pais'];
        //die($_POST['pais']);
        $empresa=$_POST['empresa'];
        $email=$_POST['email'];
        $estado=$_POST['estado'];
        $observacion=$_POST['observacion'];
        $date_lanzamiento=$_POST['date_lanzamiento'];
        $consola='';
        $pago='';
        //$consola=$_POST['consola'];
        //$pago=$_POST['pago'];

        $genero=$_POST['genero'];
        $edad=$_POST['edad'];

        $r_nombre=validate_nombre($nombre);
        $r_pais=validate_pais($pais);

        $r_empresa=validate_empresa($empresa);
        $r_email=validate_email($email);
        $r_estado=validate_estado($estado);
        $r_observacion=validate_observacion($observacion);
        $r_date_lanzamiento=validate_fecha($date_lanzamiento);
      //  $r_consola=validate_consola($consola);
        //$r_pago=validate_pago($pago);
        $r_genero=validate_genero($genero);
        $r_edad=validate_edad($edad);

        if($r_nombre !== 1){
            $e_nombre = " * El Nombre introducido no es valido ola k ase ";
            $check=false;
        }else{
            $e_nombre = "";
        }
        if($r_pais !== 1){
            $e_pais = " * El pais introducido no es valida";
            $check=false;
        }else{
            $e_pais = "";
        }
        if($r_empresa !== 1){
            $e_empresa = " * La empresa introducido no es valido";
            $check=false;
        }else{
            $e_empresa = "";
        }
        if($r_email !== 1){
            $e_email = " * El E-mail introducido no es valido";
            $check=false;
        }else{
            $e_email = "";
        }

        if(!$r_estado){
            $e_estado = " * El estado del juego introducio no es valido";
            $check=false;
        }else{
            $e_estado = "";
        }
        if(!$r_observacion){
            $e_observacion = " * La observacion no es valida";
            $check=false;
        }else{
            $e_observacion = "";
        }
        if(!$r_date_lanzamiento){
            $e_date_lanzamiento = " * No has introducido ninguna fecha";
            $check=false;
        }else{
            $e_date_lanzamiento = "";
        }
      //  if(isset($_POST["consola"]) && $_POST["consola"]=="on"){
          //$resultado="Se han aceptado las condiciones correctamente";
      //  }elseif(isset($_POST["create"])){
        //  $error="Tienes que seleccionar el check para aceptar las condiciones";
      //}

    /*  if($r_consola){
            $e_consola = " * No has seleccionado ninguna consola";
            $check=false;
        }else{
            $e_consola = "";
        }
        if($r_pago){
            $e_pago = " * No has seleccionado ninguna forma de pago";
            $check=false;
        }else{
            $e_pago = "";
        }

*/
        if(!$r_genero){
            $e_genero = " * No has seleccionado ningun Género";
            $check=false;
        }else{
            $e_genero = "";
        }
        if(!$r_edad){
            $e_edad = " * No has seleccionado ninguna edad";
            $check=false;
        }else{
            $e_edad = "";
        }
        $error = array(
                            'nombre' => $e_nombre,
                            'pais' => $e_pais,
                            'empresa' => $e_empresa,
                            'email' => $e_email,
                            'estado' => $e_estado,
                            'observacion' => $e_observacion,
                            'date_lanzamiento' => $e_date_lanzamiento,
                          //  'consola' => $e_consola,
                          //  'pago' => $e_pago,
                            'genero' => $e_genero,
                            'edad' => $e_edad
                        );
        $resultado=array('resultado'=>$check , 'error'=>$error);
        return $resultado;

        //return $check;

    }
