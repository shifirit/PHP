<div id="contenido">
    <form autocomplete="on" method="post" name="update_juego" id="update_juego" onsubmit="return validate_game();" action="index.php?page=controller_juegos&op=update">
        <h1>Modificar un juego</h1>
        <table border='0'>
            <tr>
                <td>Nombre del Juego: </td>
                <td><input type="text" id="nombre" name="nombre" placeholder="nombre" value="<?php echo $user['nombre'];?>" readonly/></td>
                <td><font color="red">
                    <span id="e_nombre" class="error">
                        <?php
                            echo $error['nombre']
                        ?>
                    </span>
                </font></font></td>
            </tr>

            <tr>
                <td>Pais de venta: </td>
                <td><input type="text" id="pais" name="pais" placeholder="pais" value="<?php echo $user['pais'];?>"/></td>
                <td><font color="red">
                    <span id="e_pais" class="error">
                        <?php
                            echo $error['pais']
                        ?>
                    </span>
                </font></font></td>
            </tr>

            <tr>
                <td>Empresa desarroladora del Videojuego: </td>
                <td><input type="text" id="empresa" name="empresa" placeholder="empresa" value="<?php echo $user['empresa'];?>"/></td>
                <td><font color="red">
                    <span id="e_empresa" class="error">
                        <?php
                            echo $error['empresa']
                        ?>
                    </span>
                </font></font></td>
            </tr>

            <tr>
                <td>E-mail: </td>
                <td><input type="text" id="email" name="email" placeholder="email" value="<?php echo $user['email'];?>"/></td>
                <td><font color="red">
                    <span id="e_email" class="error">
                        <?php
                            echo $error['email']
                        ?>
                    </span>
                </font></font></td>
            </tr>

            <tr>
                <td>Estado del Videojuego: </td>
                <td><input type="text" id="estado" name="estado" placeholder="estado" value="<?php echo $user['estado'];?>"/></td>
                <td><font color="red">
                    <span id="e_estado" class="error">
                        <?php
                            echo $error['estado']
                        ?>
                    </span>
                </font></font></td>
            </tr>

            <tr>
                <td>Breve observacion: </td>
                <td><textarea cols="30" rows="5" id="observacion" name="observacion" placeholder="observacion"><?php echo $user['observacion'];?></textarea></td>
                <td><font color="red">
                    <span id="e_observacion" class="error">
                        <?php
                            echo $error['observacion']
                        ?>
                    </span>
                </font></font></td>
            </tr>
            <tr>
                <td>Fecha de Lanzamiento del Videojuego: </td>
                <td><input id="date_lanzamiento" type="text" name="date_lanzamiento" placeholder="Fecha de Lanzamiento" readonly="readonly" value="<?php echo $user['fecha'];?>"/></td>
                <td><font color="red">
                    <span id="e_date_lanzamiento" class="error">
                        <?php
                            echo $error['date_lanzamiento']
                        ?>
                    </span>
                </font></font></td>
            </tr>

            <tr>
                <td>Plataforma Disponible: </td>
                <?php
                    $console=explode(":", $user['consola']);
                ?>
                  <td>
                    <?php
                        $busca_array=in_array("XBOX 360", $console);
                        if($busca_array){
                    ?>
                      <input type="checkbox" id= "consola[]" name="consola[]" value="Xbox 360" checked/>XBOX 360
                    <?php
                        }else{
                    ?>
                      <input type="checkbox" id= "consola[]" name="consola[]" value="Xbox 360"/>XBOX 360
                    <?php
                        }
                    ?>

                    <?php
                        $busca_array=in_array("PS4", $console);
                        if($busca_array){
                    ?>
                      <input type="checkbox" id= "consola[]" name="consola[]" value="PS4" checked/>PS4
                    <?php
                        }else{
                    ?>
                      <input type="checkbox" id= "consola[]" name="consola[]" value="PS4"/>PS4
                    <?php
                        }
                    ?>
                    <?php
                        $busca_array=in_array("Nintendo Switch", $console);
                        if($busca_array){
                    ?>
                      <input type="checkbox" id= "consola[]" name="consola[]" value="Nintendo Switch" checked/>Nintendo Switch
                    <?php
                        }else{
                    ?>
                        <input type="checkbox" id= "consola[]" name="consola[]" value="Nintendo Switch" />Nintendo Switch
                    <?php
                        }
                    ?>

                    <?php
                        $busca_array=in_array("Wii U", $console);
                        if($busca_array){
                    ?>
                      <input type="checkbox" id= "consola[]" name="consola[]" value="Wii U" checked/>Wii U
                    <?php
                        }else{
                    ?>
                      <input type="checkbox" id= "consola[]" name="consola[]" value="Wii U" />Wii U
                    <?php
                        }
                    ?>

                    <?php
                        $busca_array=in_array("PSP ", $console);
                        if($busca_array){
                    ?>
                      <input type="checkbox" id= "consola[]" name="consola[]" value="PSP" checked/>PSP
                    <?php
                        }else{
                    ?>
                        <input type="checkbox" id= "consola[]" name="consola[]" value="PSP" />PSP
                    <?php
                        }
                    ?>

                    <?php
                        $busca_array=in_array("Nintendo DS", $console);
                        if($busca_array){
                    ?>
                      <input type="checkbox" id= "consola[]" name="consola[]" value="Nintendo DS" checked/>Nintendo DS
                    <?php
                        }else{
                    ?>
                      <input type="checkbox" id= "consola[]" name="consola[]" value="Nintendo DS" />Nintendo DS
                    <?php
                        }
                    ?>

                    <?php
                        $busca_array=in_array("PC", $console);
                        if($busca_array){
                    ?>
                        <input type="checkbox" id= "consola[]" name="consola[]" value="PC" checked/>PC
                    <?php
                        }else{
                    ?>
                        <input type="checkbox" id= "consola[]" name="consola[]" value="PC" />PC
                    <?php
                        }
                    ?>

                    <?php
                        $busca_array=in_array("Otros", $console);
                        if($busca_array){
                    ?>
                        <input type="checkbox" id= "consola[]" name="consola[]" value="Otros" checked/>Otros
                    <?php
                        }else{
                    ?>
                        <input type="checkbox" id= "consola[]" name="consola[]" value="Otros" />Otros
                    <?php
                        }
                    ?>

                </td>
                <td><font color="red">
                    <span id="e_consola" class="error">

                    </span>
                </font></font></td>
            </tr>



            <tr>
                <td>Forma de Pago: </td>
                <?php
                    $pay=explode(":", $user['pago']);
                ?>
                <td>
                    <?php
                        $busca_array=in_array("PayPal", $pay);
                        if($busca_array){
                    ?>
                        <input type="checkbox" id= "pago[]" name="pago[]" value="PayPal" checked/>PayPal
                    <?php
                        }else{
                    ?>
                        <input type="checkbox" id= "pago[]" name="pago[]" value="PayPal"/>PayPal
                    <?php
                        }
                    ?>
                    <?php
                        $busca_array=in_array("VISA", $pay);
                        if($busca_array){
                    ?>
                        <input type="checkbox" id= "pago[]" name="pago[]" value="VISA" checked/>VISA
                    <?php
                        }else{
                    ?>
                        <input type="checkbox" id= "pago[]" name="pago[]" value="VISA"/>VISA
                    <?php
                        }
                    ?>
                    <?php
                        $busca_array=in_array("Transferencia Bancaria", $pay);
                        if($busca_array){
                    ?>
                        <input type="checkbox" id= "pago[]" name="pago[]" value="Transferencia Bancaria" checked/>Transferencia Bancaria
                    <?php
                        }else{
                    ?>
                    <input type="checkbox" id= "pago[]" name="pago[]" value="Transferencia Bancaria"/>Transferencia Bancaria
                    <?php
                        }
                    ?>

                    <?php
                        $busca_array=in_array("Otros", $pay);
                        if($busca_array){
                    ?>
                        <input type="checkbox" id= "pago[]" name="pago[]" value="Otros" checked/>Otros
                    <?php
                        }else{
                    ?>
                    <input type="checkbox" id= "pago[]" name="pago[]" value="Otros"/>Otros
                    <?php
                        }
                    ?>

                </td>
                <td><font color="red">
                    <span id="e_pago" class="error">
                        
                    </span>
                </font></font></td>
            </tr>


            <tr>
                <td>Género: </td>
                <td><select id="genero" name="genero" placeholder="pais">
                    <?php
                        if($user['genero']==="Accion"){
                    ?>
                      <option value="Accion" selected>Accion</option>
                      <option value="Disparos">Disparos</option>
                      <option value="Estrategia">Estrategia</option>
                      <option value="Simulacion">Simulacion</option>
                      <option value="Deporte">Deporte</option>
                      <option value="Carreras">Carreras</option>
                      <option value="Aventura">Aventura</option>
                      <option value="Rol">Rol</option>
                      <option value="Otros">Otros</option>
                    <?php
                        }elseif($user['genero']==="Disparos"){
                    ?>
                    <option value="Accion">Accion</option>
                    <option value="Disparos" selected>Disparos</option>
                    <option value="Estrategia">Estrategia</option>
                    <option value="Simulacion">Simulacion</option>
                    <option value="Deporte">Deporte</option>
                    <option value="Carreras">Carreras</option>
                    <option value="Aventura">Aventura</option>
                    <option value="Rol">Rol</option>
                    <option value="Otros">Otros</option>

                    <?php
                        }elseif($user['genero']==="Estrategia"){
                    ?>
                    <option value="Accion">Accion</option>
                    <option value="Disparos" >Disparos</option>
                    <option value="Estrategia" selected>Estrategia</option>
                    <option value="Simulacion">Simulacion</option>
                    <option value="Deporte">Deporte</option>
                    <option value="Carreras">Carreras</option>
                    <option value="Aventura">Aventura</option>
                    <option value="Rol">Rol</option>
                    <option value="Otros">Otros</option>

                    <?php
                        }elseif($user['genero']==="Simulacion"){
                    ?>
                    <option value="Accion">Accion</option>
                    <option value="Disparos">Disparos</option>
                    <option value="Estrategia">Estrategia</option>
                    <option value="Simulacion" selected>Simulacion</option>
                    <option value="Deporte">Deporte</option>
                    <option value="Carreras">Carreras</option>
                    <option value="Aventura">Aventura</option>
                    <option value="Rol">Rol</option>
                    <option value="Otros">Otros</option>

                    <?php
                        }elseif($user['genero']==="Deporte"){
                    ?>
                    <option value="Accion">Accion</option>
                    <option value="Disparos">Disparos</option>
                    <option value="Estrategia">Estrategia</option>
                    <option value="Simulacion">Simulacion</option>
                    <option value="Deporte" selected>Deporte</option>
                    <option value="Carreras">Carreras</option>
                    <option value="Aventura">Aventura</option>
                    <option value="Rol">Rol</option>
                    <option value="Otros">Otros</option>

                    <?php
                        }elseif($user['genero']==="Carreras"){
                    ?>
                    <option value="Accion">Accion</option>
                    <option value="Disparos">Disparos</option>
                    <option value="Estrategia">Estrategia</option>
                    <option value="Simulacion">Simulacion</option>
                    <option value="Deporte">Deporte</option>
                    <option value="Carreras" selected>Carreras</option>
                    <option value="Aventura">Aventura</option>
                    <option value="Rol">Rol</option>
                    <option value="Otros">Otros</option>

                    <?php
                        }elseif($user['genero']==="Aventura"){
                    ?>
                    <option value="Accion">Accion</option>
                    <option value="Disparos">Disparos</option>
                    <option value="Estrategia">Estrategia</option>
                    <option value="Simulacion">Simulacion</option>
                    <option value="Deporte">Deporte</option>
                    <option value="Carreras" >Carreras</option>
                    <option value="Aventura" selected>Aventura</option>
                    <option value="Rol">Rol</option>
                    <option value="Otros">Otros</option>

                    <?php
                        }elseif($user['genero']==="Rol"){
                    ?>
                    <option value="Accion">Accion</option>
                    <option value="Disparos">Disparos</option>
                    <option value="Estrategia">Estrategia</option>
                    <option value="Simulacion">Simulacion</option>
                    <option value="Deporte">Deporte</option>
                    <option value="Carreras">Carreras</option>
                    <option value="Aventura">Aventura</option>
                    <option value="Rol" selected>Rol</option>
                    <option value="Otros">Otros</option>

                    <?php
                        }else{
                    ?>
                    <option value="Accion">Accion</option>
                    <option value="Disparos">Disparos</option>
                    <option value="Estrategia">Estrategia</option>
                    <option value="Simulacion">Simulacion</option>
                    <option value="Deporte">Deporte</option>
                    <option value="Carreras">Carreras</option>
                    <option value="Aventura">Aventura</option>
                    <option value="Rol">Rol</option>
                    <option value="Otros" selected>Otros</option>
                    <?php
                        }
                    ?>
                    </select></td>
                <td><font color="red">
                    <span id="e_genero" class="error">
                        <?php
                            echo $error['genero']
                        ?>
                    </span>
                </font></font></td>
            </tr>


            <tr>
                <td>Edad recomendada del videojuego: </td>
                <td>
                    <?php
                  if ($user['edad']==="tp"){
                    ?>
                    <input type="radio" id="edad" name="edad" placeholder="edad" value="tp" checked/>Todos los publicos
                    <input type="radio" id="edad" name="edad" placeholder="edad" value="+3"/>+3
                    <input type="radio" id="edad" name="edad" placeholder="edad" value="+12"/>+12
                    <input type="radio" id="edad" name="edad" placeholder="edad" value="+18"/>+18

                    <?php
                  }elseif ($user['edad']==="+3") {
                    ?>
                    <input type="radio" id="edad" name="edad" placeholder="edad" value="tp" />Todos los publicos
                    <input type="radio" id="edad" name="edad" placeholder="edad" value="+3" checked/>+3
                    <input type="radio" id="edad" name="edad" placeholder="edad" value="+12"/>+12
                    <input type="radio" id="edad" name="edad" placeholder="edad" value="+18"/>+18

                    <?php
                  }elseif ($user['edad']==="+12") {
                    ?>
                    <input type="radio" id="edad" name="edad" placeholder="edad" value="tp" />Todos los publicos
                    <input type="radio" id="edad" name="edad" placeholder="edad" value="+3" />+3
                    <input type="radio" id="edad" name="edad" placeholder="edad" value="+12" checked />+12
                    <input type="radio" id="edad" name="edad" placeholder="edad" value="+18"/>+18

                    <?php
                  }else{
                    ?>
                    <input type="radio" id="edad" name="edad" placeholder="edad" value="tp" />Todos los publicos
                    <input type="radio" id="edad" name="edad" placeholder="edad" value="+3" />+3
                    <input type="radio" id="edad" name="edad" placeholder="edad" value="+12" />+12
                    <input type="radio" id="edad" name="edad" placeholder="edad" value="+18" checked />+18
                    <?php
                        }
                    ?>
                </td>
                <td><font color="red">
                    <span id="e_edad" class="error">
                        <?php
                            echo $error['edad']
                        ?>
                    </span>
                </font></font></td>
            </tr>

            <tr>
                <td><input type="submit" name="update" id="update"/></td>
                <td align="right"><a href="index.php?page=controller_juegos&op=list">Volver</a></td>
            </tr>
        </table>
    </form>
</div>
