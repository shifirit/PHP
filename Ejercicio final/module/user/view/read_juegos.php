<div id="contenido">
    <h1>Informacion del Juego</h1>
    <p>
    <table border='2'>
        <tr>
            <td>Nombre del Videojuego: </td>
            <td>
                <?php
                    echo $user['nombre'];

                ?>
            </td>
        </tr>

        <tr>
            <td>Pais de venta: </td>
            <td>
                <?php
                    echo $user['pais'];
                ?>
            </td>
        </tr>

        <tr>
            <td>Empresa de desarrollo: </td>
            <td>
                <?php
                    echo $user['empresa'];
                ?>
            </td>
        </tr>

        <tr>
            <td>E-mail: </td>
            <td>
                <?php
                    echo $user['email'];
                ?>
            </td>
        </tr>

        <tr>
            <td>Estado: </td>
            <td>
                <?php
                    echo $user['estado'];
                ?>
            </td>
        </tr>

        <tr>
            <td>Observacion: </td>
            <td>
                <?php
                    echo $user['observacion'];
                ?>
            </td>
        </tr>

        <tr>
            <td>Fecha de Lanzamiento: </td>
            <td>
                <?php
                    echo $user['fecha'];
                ?>
            </td>
        </tr>

        <tr>
            <td>Plataforma disponible: </td>
            <td>
                <?php
                    echo $user['consola'];
                ?>
            </td>

        </tr>

        <tr>
            <td>Forma de pago: </td>
            <td>
                <?php
                    echo $user['pago'];
                ?>
            </td>
        </tr>

        <tr>
            <td>Género: </td>
            <td>
                <?php
                    echo $user['genero'];
                ?>
            </td>
        </tr>

        <tr>
            <td>Edad recomendada del videojuego: </td>
            <td>
                <?php
                    echo $user['edad'];
                ?>
            </td>
        </tr>
    </table>
    </p>
    <p><a href="index.php?page=controller_juegos&op=list">Volver</a></p>
</div>
