<div id="contenido">
    <form autocomplete="on" method="post" name="registro_juego" id="registro_juego" onsubmit="return validate_game();" action="index.php?page=controller_juegos&op=create">
        <h1>Nuevo Juego</h1>
        <table border='0'>
            <tr>
                <td>Nombre del videojuego: </td>
                <td><input type="text" size="30" id="nombre" name="nombre" placeholder="Escibre el nombre del juego" value="<?php echo $_POST?$_POST['nombre']:""; ?>"/></td>
                <td><font color="red">
                    <span id="e_nombre" class="error">
                        <?php
                            echo $error['nombre']
                        ?>
                    </span>
                </font></font></td>
            </tr>

            <tr>
                <td>Pais de venta: </td>
                <td><input type="text" size="30" id="pais" name="pais" placeholder="Pais de venta" value="<?php echo $_POST?$_POST['pais']:""; ?>"/></td>
                <td><font color="red">
                    <span id="e_pais" class="error">
                        <?php
                            echo $error['pais']
                        ?>
                    </span>
                </font></font></td>
            </tr>

            <tr>
                <td>Empresa desarroladora del juego: </td>
                <td><input type="text" size="30" id="empresa" name="empresa" placeholder="Empresa desarrolladora del juego" value="<?php echo $_POST?$_POST['empresa']:""; ?>"/></td>
                <td><font color="red">
                    <span id="e_empresa" class="error">
                        <?php
                            echo $error['empresa']
                        ?>
                    </span>
                </font></font></td>
            </tr>

            <tr>
                <td>Email: </td>
                <td><input type="text" size="30" id= "email" name="email" placeholder="Escriba su email" value="<?php echo $_POST?$_POST['email']:""; ?>"/></td>
                <td><font color="red">
                    <span id="e_email" class="error">
                        <?php
                            echo $error['email']
                        ?>
                    </span>
                </font></font></td>
            </tr>
            <tr>
                <td>Estado del juego: </td>
                <td><input type="text" size="30" id= "estado" name="estado" placeholder="Estado del juego" value="<?php echo $_POST?$_POST['estado']:""; ?>"/></td>
                <td><font color="red">
                    <span id="e_estado" class="error">
                        <?php
                            echo $error['estado']
                        ?>
                    </span>
                </font></font></td>
            </tr>
            <tr>
                <td>Breve observacion: </td>
                <td><textarea rows="5" cols="50" type="text" id= "observacion" name="observacion" placeholder="Breve observación" value="<?php echo $_POST?$_POST['observacion']:""; ?>"/></textarea></td>
                <td><font color="red">
                    <span id="e_observacion" class="error">
                        <?php
                            echo $error['observacion']
                        ?>
                    </span>
                </font></font></td>
            </tr>

            <tr>
                <td>Fecha de Lanzamiento del juego: </td>
                <td><input id="date_lanzamiento" size="30" type="text" name="date_lanzamiento" placeholder="Seleccione una Fecha de Lanzamiento del juego" value="<?php echo $_POST?$_POST['date_lanzamiento']:""; ?>" readonly="readonly"/></td>
                <td><font color="red">
                    <span id="e_date_lanzamiento" class="error">
                        <?php
                            echo $error['date_lanzamiento']
                        ?>
                    </span>
                </font></font></td>
            </tr>

            <tr>
                <td>Plataforma disponible: </td>
                <td><input type="checkbox" id= "consola[]" name="consola[]" placeholder= "consola" value="Xbox 360"/>XBOX 360
                    <input type="checkbox" id= "consola[]" name="consola[]" placeholder= "consola" value="PS4"/>PS4
                    <input type="checkbox" id= "consola[]" name="consola[]" placeholder= "consola" value="Nintendo Switch"/>Nintendo Switch
                    <input type="checkbox" id= "consola[]" name="consola[]" placeholder= "consola" value="Wii U"/>Wii U
                    <input type="checkbox" id= "consola[]" name="consola[]" placeholder= "consola" value="PSP"/>PSP
                    <input type="checkbox" id= "consola[]" name="consola[]" placeholder= "consola" value="Nintendo DS"/>Nintendo DS
                    <input type="checkbox" id= "consola[]" name="consola[]" placeholder= "consola" value="PC"/>PC
                    <input type="checkbox" id= "consola[]" name="consola[]" placeholder= "consola" value="Otros" checked/>Otros</td>
                <td><font color="red">
                    <span id="e_consola" class="error">


                    </span>
                </font></font></td>
            </tr>

            <tr>
                <td>Forma de Pago: </td>
                <td><input type="checkbox" id= "pago[]" name="pago[]" placeholder= "pago" value="PayPal"/>PayPal
                    <input type="checkbox" id= "pago[]" name="pago[]" placeholder= "pago" value="VISA"/>VISA
                    <input type="checkbox" id= "pago[]" name="pago[]" placeholder= "pago" value="Transferencia Bancaria"/>Transferencia Bancaria
                    <input type="checkbox" id= "pago[]" name="pago[]" placeholder= "pago" value="Otros" checked />Otros</td>
                <td><font color="red">
                    <span id="e_pago" class="error">

                    </span>
                </font></font></td>
            </tr>


            <tr>
                <td>Género: </td>
                <td><select id="genero" name="genero" placeholder="genero">
                  <option value="Accion"selected >Acción</option>
        					<option value="Disparos">Disparos</option>
        					<option value="Estrategia">Estrategia</option>
        					<option value="Simulacion">Simulación</option>
        					<option value="Deporte">Deporte</option>
        					<option value="Carreras">Carreras</option>
        					<option value="Aventura">Aventura</option>
        					<option value="Rol">Rol</option>
        					<option value="Otros">Otros</option>
                    </select></td>
                <td><font color="red">
                    <span id="e_genero" class="error">
                        <?php
                            echo $error['genero']
                        ?>
                    </span>
                </font></font></td>
            </tr>

            <tr>
                <td>Edad recomendada del videojuego: </td>
                <td><input type="radio" id="edad" name="edad" placeholder="edad" value="tp" checked/>tp
                    <input type="radio" id="edad" name="edad" placeholder="edad" value="+3"/>+3
                    <input type="radio" id="edad" name="edad" placeholder="edad" value="+12"/>+12
                    <input type="radio" id="edad" name="edad" placeholder="edad" value="+18"/>+18</td>

                <td><font color="red">
                    <span id="e_edad" class="error">
                        <?php
                            echo $error['edad']
                        ?>
                    </span>
                </font></font></td>
            </tr>

            <tr>
                <td><input type="submit" name="create" id="create"/></td>
                <td align="right"><a href="index.php?page=controller_juegos&op=list">Volver</a></td>
            </tr>
        </table>
    </form>
</div>
