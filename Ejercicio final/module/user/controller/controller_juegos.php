<?php
    include ("module/user/model/DAOJuegos.php");
    //session_start();

    switch($_GET['op']){
        case 'list';
            try{
                $daouser = new Videojuegos();
            	$rdo = $daouser->select_all_user();

            }catch (Exception $e){
                $callback = 'index.php?page=503';
			    die('<script>window.location.href="'.$callback .'";</script>');
            }

            if(!$rdo){
    			$callback = 'index.php?page=503';
			    die('<script>window.location.href="'.$callback .'";</script>');
    		}else{
                include("module/user/view/list_juegos.php");
    		}
            break;

        case 'create';
            include("module/user/model/validate.php");

              $check = true;
              $error = array(
                                'nombre' => '',
                                'pais' => '',
                                'empresa' => '',
                                'email' => '',
                                'estado' => '',
                                'observacion' => '',
                                'date_lanzamiento' => '',
                              //  'consola' =>'',
                            //    'pago' => '',
                                'genero' => '',
                                'edad' => ''
                        );

                if (isset($_POST['create'])){
                    $result=validate();
                    //if ($check){
                    if ($result['resultado']) {
                        $_SESSION['user']=$_POST;
                        //print_r($_SESSION['user']);
                      //  die();
                        //die($_SESSION['']);
                        try{
                            $daouser = new Videojuegos();
                            $rdo = $daouser->insert_user($_POST);
                        }catch (Exception $e){
                            $callback = 'index.php?page=503';
                            die('<script>window.location.href="'.$callback .'";</script>');
                        }

                        if($rdo){
                            echo '<script language="javascript">alert("Actualizado en la base de datos correctamente")</script>';
                            $callback = 'index.php?page=controller_juegos&op=list';
                            die('<script>window.location.href="'.$callback .'";</script>');
                        }else{
                            $callback = 'index.php?page=503';
                            die('<script>window.location.href="'.$callback .'";</script>');
                        }
                    }
                    else {
                        $error = $result['error'];
                    }
                }
                include("module/user/view/create_juegos.php");
                break;



        case 'update';
            include("module/user/model/validate.php");

            $check = true;
            $error = array(
                              'nombre' => '',
                              'pais' => '',
                              'empresa' => '',
                              'email' => '',
                              'estado' => '',
                              'observacion' => '',
                              'date_lanzamiento' => '',
                            //  'consola' =>'',
                            //  'pago' => '',
                              'genero' => '',
                              'edad' => ''
                    );
                    if (isset($_POST['update'])){
                      //print_r($_POST);
                    // die();
                        $result=validate();

                        //print_r($result);
                      //  die();

                        if ($result['resultado']) {
                          //print_r($result);
                        // die();
                            $_SESSION['user']=$_POST;
                          //  print_r($_POST);
                            //die();
                            try{
                              $daouser = new Videojuegos();

                              $rdo = $daouser->update_user($_POST);

                            //  print_r($rdo);
                            //  die();
                            }catch (Exception $e){
                                $callback = 'index.php?page=503';
                                die('<script>window.location.href="'.$callback .'";</script>');
                            }

                            if($rdo){
                              echo '<script language="javascript">alert("Actualizado en la base de datos correctamente")</script>';
                              $callback = 'index.php?page=controller_juegos&op=list';
                              die('<script>window.location.href="'.$callback .'";</script>');
                            }else{
                              $callback = 'index.php?page=503';
                              die('<script>window.location.href="'.$callback .'";</script>');
                            }
                        }
                        else {
                            $error = $result['error'];
                            $_SESSION['user']=$_POST;
                            $_GET['id']=$_POST['nombre'];
                        }
                    }



                    try{
                        $daouser = new Videojuegos();
                      //  print_r($_GET['id']);
                      $rdo = $daouser->select_user($_GET['id']);
                      $user=get_object_vars($rdo);
                    }catch (Exception $e){
                        $callback = 'index.php?page=503';
                    die('<script>window.location.href="'.$callback .'";</script>');
                    }

                    if(!$rdo){
                    $callback = 'index.php?page=503';
                    die('<script>window.location.href="'.$callback .'";</script>');
                    }else{

                        $id=$_GET['id'];
                      include("module/user/view/update_juegos.php");
                    }
                    break;

      case 'read';
          $daouser = new Videojuegos();
              $rdo = $daouser->select_user($_GET['id']);
              $user = get_object_vars($rdo);


              if ($rdo) {
                  include("module/user/view/read_juegos.php");
                  }
                    break;


        case 'delete';
            if (isset($_POST['delete'])){
                try{
                    $daouser = new Videojuegos();
                	$rdo = $daouser->delete_user($_GET['id']);
                }catch (Exception $e){
                    $callback = 'index.php?page=503';
    			    die('<script>window.location.href="'.$callback .'";</script>');
                }

            	if($rdo){
        			echo '<script language="javascript">alert("Borrado en la base de datos correctamente")</script>';
        			$callback = 'index.php?page=controller_juegos&op=list';
    			    die('<script>window.location.href="'.$callback .'";</script>');
        		}else{
        			$callback = 'index.php?page=503';
			        die('<script>window.location.href="'.$callback .'";</script>');
        		}
            }

            include("module/user/view/delete_juegos.php");
            break;

            case 'delete_all';
             if (isset($_POST['delete_all'])){
                  try{
                      $daouser = new Videojuegos();
                      $rdo = $daouser->delete_all();
                  }catch (Exception $e){
                      $callback = 'index.php?page=503';
                      die('<script>window.location.href="'.$callback .'";</script>');
                  }
                  if($rdo){
                      echo '<script language="javascript">alert("Borrado de la base de datos correctamente")</script>';
                      print_r($rdo);
                      $callback = 'index.php?page=controller_juegos&op=list';
                      die('<script>window.location.href="'.$callback .'";</script>');
                  }else{
                      $callback = 'index.php?page=503';
                      die('<script>window.location.href="'.$callback .'";</script>');
                  }
                   }

              include("module/user/view/delete_all.php");
              break;
        default;
            include("view/inc/error404.php");
            break;
    }
