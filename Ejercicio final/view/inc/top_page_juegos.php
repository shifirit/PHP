<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Venta de un juego</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.css" />
    	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery.js"></script>
    	<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.js"></script>
    	<script type="text/javascript">
      $(function() {
        $('#date_lanzamiento').datepicker({
          dateFormat: 'dd/mm/yy',
          changeMonth: true,
          changeYear: true,
          minDate: new Date(1950,1-1,1),maxDate:'-1Y',
          defaultDate:new Date(2000,1-1,1),
          yearRange:'-90:-1'
        //  onSelect: function(selectedDate) {
            //alert(selectedDate);
          //}
        });
      });


	    </script>
	    <link href="view/css/style.css" rel="stylesheet" type="text/css" />
	    <script src="module/user/model/validate_juegos.js"></script>
    </head>
    <body>
