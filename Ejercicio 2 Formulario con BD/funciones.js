/*$(function() {
		$( "input:submit", ".demo" ).button();
		$( ".demo" ).click(function() { return false; });
	});
*/
//Aquí comenzamos creando un plugin
jQuery.fn.LlenarLimpiarCampos = function(){
	this.each(function(){
		$(".nom").attr("value","Introduzca su nombre");
		$(".nom").focus(function(){
			if($(".nom").attr("value")=="Introduzca su nombre"){
			$(".nom").attr("value","");
			}
		});
		$(".nom").blur(function(){
			if($(".nom").attr("value")==""){
		   $(".nom").attr("value","Introduzca su nombre");
			}
		});

		$(".apellido").attr("value","Introduzca su apellido");
	$(".apellido").focus(function(){
		if($(".apellido").attr("value")=="Introduzca su apellido"){
		$(".apellido").attr("value","");
		}
	});
	$(".apellido").blur(function(){
		if($(".apellido").attr("value")==""){
		 $(".apellido").attr("value","Introduzca su apellido");
		}
	});

	$(".nacionalidad").attr("value","Introduzca su nacionalidad");
$(".nacionalidad").focus(function(){
	if($(".nacionalidad").attr("value")=="Introduzca su nacionalidad"){
	$(".nacionalidad").attr("value","");
	}
});
$(".nacionalidad").blur(function(){
	if($(".nacionalidad").attr("value")==""){
	 $(".nacionalidad").attr("value","Introduzca su nacionalidad");
	}
});

		$(".email").attr("value","Introduzca su email");
		$(".email").focus(function(){
			if($(".email").attr("value")=="Introduzca su email"){
			$(".email").attr("value","");
			}
		});
		$(".email").blur(function(){
			if($(".email").attr("value")==""){
		   $(".email").attr("value","Introduzca su email");
			}
		});

		$(".nick").attr("value","Introduzca su nick");
		$(".nick").focus(function(){
			if($(".nick").attr("value")=="Introduzca su nick"){
			$(".nick").attr("value","");
			}
		});
		$(".nick").blur(function(){
			if($(".nick").attr("value")==""){
		   $(".nick").attr("value","Introduzca su nick");
			}
		});


			$(".pass").attr("value","Introduzca su password");
		$(".pass").focus(function(){
			if($(".pass").attr("value")=="Introduzca su password"){
			$(".pass").attr("value","");
			}
		});
		$(".pass").blur(function(){
			if($(".pass").attr("value")==""){
		   $(".pass").attr("value","Introduzca su password");
			}
		});

		$(".password").attr("value","Introduzca su password");
	$(".password").focus(function(){
		if($(".password").attr("value")=="Introduzca su password"){
		$(".password").attr("value","");
		}
	});
	$(".password").blur(function(){
		if($(".password").attr("value")==""){
		 $(".password").attr("value","Introduzca su password");
		}
	});

	$(".datepiker").attr("value","Click para seleccionar una fecha");
$(".datepiker").focus(function(){
	if($(".datepiker").attr("value")=="Click para seleccionar una fecha"){
	$(".datepiker").attr("value","");
	}
});
$(".datepiker").blur(function(){
	if($(".datepiker").attr("value")==""){
	 $(".datepiker").attr("value","Click para seleccionar una fecha");
	}
});

});

	return this;
};

$(document).ready(function () {
	$(this).LlenarLimpiarCampos();

    var emailreg = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
    $(".boton").click(function(){
    	$(".error").remove();

		if( $(".nom").val() == "" || $(".nom").val() == "Introduzca su nombre"){
			$(".nom").focus().after("<span class='error'>Ingrese su nombre</span>");
			return false;
		}else if($(".nom").val().length < 2){
			$(".nom").focus().after("<span class='error'>Mínimo 2 carácteres para el nombre</span>");
			return false;

		}else if( $(".apellido").val() == "" || $(".apellido").val() == "Introduzca su apellido"){
			$(".apellido").focus().after("<span class='error'>Ingrese un apellido</span>");
			return false;
		}else if($(".apellido").val().length < 20){
			$(".apellido").focus().after("<span class='error'>Mínimo 20 carácteres para el apellido</span>");
			return false;

		}else if( $(".nacionalidad").val() == "" || $(".nacionalidad").val() == "Introduzca su nacionalidad"){
			$(".nacionalidad").focus().after("<span class='error'>Ingrese su nacionalidad</span>");
			return false;
		}else if($(".nacionalidad").val().length < 2){
			$(".nacionalidad").focus().after("<span class='error'>Mínimo 2 carácteres para la nacionalidad</span>");
			return false;

		}else if( $(".email").val() == "" ||  !emailreg.test($(".email").val()) || $(".email").val() == "Introduzca su email"){
			$(".email").focus().after("<span class='error'>Ingrese un email correcto</span>");
			return false;

		}else if( $(".nick").val() == "" || $(".nick").val() == "Introduzca su nick"){
			$(".nick").focus().after("<span class='error'>Ingrese un nick</span>");
			return false;
		}else if($(".nick").val().length < 5){
			$(".nick").focus().after("<span class='error'>Mínimo 5 carácteres para el nick</span>");
			return false;

		}else if( $(".pass").val() == "" || $(".pass").val() == "Introduzca su password"){
			$(".pass").focus().after("<span class='error'>Ingrese una password</span>");
			return false;
		}else if($(".pass").val().length < 7){
			$(".pass").focus().after("<span class='error'>Mínimo 7 carácteres para la password</span>");
			return false;


		}else if( $(".password").val() == "" || $(".mensaje").val() == "Introduzca su password"){
			$(".password").focus().after("<span class='error'>Ingrese una password</span>");
			return false;
		}else if($(".password").val().length < 7){
			$(".password").focus().after("<span class='error'>Mínimo 7 carácteres para la password</span>");
			return false;

		}else if( $(".datepiker").val() == "" || $(".datepiker").val() == "Click para seleccionar la fecha"){
			$(".datepiker").focus().after("<span class='error'>Tienes que elegir una fecha </span>");
			return false;


		}
	});

	//realizamos funciones para que sea más práctico nuestro formulario
	$(".nom, .asunto, .mensaje, .apellido, .nacionalidad, .pass, .password,.nick,.datepiker").keyup(function(){
		if ( $(this).val() != "" ){
			$(".error").fadeOut();
			return false;
		}
	});

	$(".nom").keyup(function(){
		if ($(this).val().length >= 2){
			$(".error").fadeOut();
			return false;
		}
	});

	$(".email").keyup(function(){
		if ( $(this).val() != "" && emailreg.test($(this).val())){
			$(".error").fadeOut();
			return false;
		}
	});


	$(".apellido").keyup(function(){
		if ($(this).val().length >= 4){
			$(".error").fadeOut();
			return false;
		}
	});

	$(".pass").keyup(function(){
		if ($(this).val().length >= 7){
			$(".error").fadeOut();
			return false;
		}
	});

	$(".password").keyup(function(){
		if ($(this).val().length >= 7){
			$(".error").fadeOut();
			return false;
		}
	});

	$(".nick").keyup(function(){
		if ($(this).val().length >=5){
			$(".error").fadeOut();
			return false;
		}
	});

	$(".nacionalidad").keyup(function(){
		if ($(this).val().length >= 2){
			$(".error").fadeOut();
			return false;
		}
	});

  $(".datepiker").keyup(function(){
		if ($(this).val().length != ""){
			$(".error").fadeOut();
			return false;
		}
	});


});
