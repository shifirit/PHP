CREATE DATABASE comentarios;
use comentarios;
CREATE TABLE mensajes2(
    id int AUTO_INCREMENT not null PRIMARY KEY,
    nombre varchar(60),
    email varchar(100),
    asunto varchar(200),
    apellidos varchar(100),
    mensaje varchar(100),
    hora varchar(100),
    fecha varchar(100)
);
