<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//ES" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Formulario con un botón especial</title>
	<link rel="stylesheet" href="estilos.css" type="text/css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.js"></script>
	<script type="text/javascript" src="funciones.js"></script>
	<script type="text/javascript">
  $(function() {
    $('#demo1').datepicker({
      dateFormat: 'dd/mm/yy',
      changeMonth: true,
      changeYear: true,
      minDate: new Date(1900,1-1,1),maxDate:'-18Y',
			defaultDate:new Date(1970,1-1,1),
			yearRange:'-90:-18'
    //  onSelect: function(selectedDate) {
        //alert(selectedDate);
      //}
    });
	});
	</script>

</head>
<body>
	<!--<h1>User Form</h1>-->
	<?php
	session_start();
	if(isset($_POST['grabar']) and $_POST['grabar'] == 'si'){
			$error = array(); // declaramos un array para almacenar los errores
			if($_POST['nom'] == ''){
				$error[1] = '<span class="error">Ingrese su nombre</span>';
			}else if(strlen($_POST['nom']) < 3){
				$error[1] = '<span class="error">Mínimo 3 carácteres para el nombre</span>';

			}else if($_POST['apellido'] == ''){
				$error[2] = '<span class="error">Ingrese su apellido</span>';
			}else if(strlen($_POST['apellido']) < 4){
				$error[2] = '<span class="error">Mínimo 4 carácteres para el nombre</span>';

			}else if($_POST['nacionalidad'] == ''){
				$error[3] = '<span class="error">Ingrese su nacionalidad</span>';
			}else if(strlen($_POST['nacionalidad']) < 2){
				$error[3] = '<span class="error">Mínimo 2 carácteres para la nacionalidad</span>';

			}else if($_POST['email'] == '' or !preg_match("/^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/",$_POST['email'])){
				$error[4] = '<span class="error">Ingrese un email correcto</span>';

			}else if($_POST['nick'] == ''){
				$error[5] = '<span class="error">Ingrese un nick</span>';
			}else if(strlen($_POST['nick']) < 5){
				$error[5] = '<span class="error">Mínimo 5 carácteres para el nick</span>';

			}else if($_POST['pass'] == ''){
				$error[6] = '<span class="error">Ingrese una password</span>';
			}else if(strlen($_POST['pass']) < 7){
				$error[6] = '<span class="error">Mínimo 7 carácteres para la password</span>';
			}else if($_POST['password'] == ''){
				$error[7] = '<span class="error">Ingrese una password</span>';
			}else if(strlen($_POST['password']) < 7){
				$error[7] = '<span class="error">Mínimo 20 carácteres para la password</span>';

				// NO va el control
		/*	}else if($_POST['datepiker'] == ''){
				$error[8] = '<span class="error">Click para seleccionar la fecha</span>';
*/
			//}else if($_POST['mensaje'] == ''){
			//	$error[5] = '<span class="error">Ingrese un mensaje</span>';
			//}else if(strlen($_POST['mensaje']) < 20){
			//	$error[5] = '<span class="error">Mínimo 20 carácteres para el nombre</span>';



			}else{
				$_SESSION = $_POST;
				//print_r($_SESSION);
				//die();
				$url = "procesa.php";
				die('<script>window.location.href="'.$url.'";</script>');
		}
	}
	?>

	<div class="formulario">
	<form name="form" class="contacto" action="" method="post">

		<div><label>Tu Nombre:</label><input type='text' name="nom" class="nom" value="<?php @$_POST['nom'] ?>" ><?php echo @$error[1] ?></div>
		<div><label>Apellidos	:</label><input type='text' name="apellido"  class="apellido" value="<?php @$_POST['apellido'] ?>"><?php echo @$error[2] ?></div>
		<div><label>Nacionalidad	:</label><input type='text' name="nacionalidad"  class="nacionalidad" value="<?php @$_POST['nacionalidad'] ?>"><?php echo @$error[3] ?></div>
		<div><label>Tu Email:</label><input type='text' name="email"  class="email" value="<?php @$_POST['email'] ?>"><?php echo @$error[4] ?></div>
		<div><label>Nick:</label><input type='text' name="nick" class="nick" value="<?php @$_POST['nick'] ?>" ><?php echo @$error[5] ?></div>
		<div><label>Password:</label><input type='text' name="pass" class="pass" value="<?php @$_POST['pass'] ?>" ><?php echo @$error[6] ?></div>
		<div><label>Repite la Password:</label><input type='text' name="password" class="password" value="<?php @$_POST['password'] ?>" ><?php echo @$error[7] ?></div>
		<p><label>Selecciona fecha de nacimiento:</label><input id="demo1" type="text" name="datepiker" class="datepiker" readonly="readonly" value="<?php @$_POST['datepiker'] ?>"><?php echo @$error[8] ?></p>
		<div>Idiomas:
					<br>Español <input type="checkbox" name="idioma[]" value="Español" >
					Ingles <input type="checkbox" name="idioma[]" value="Ingles">
					Otros   <input type="checkbox" name="idioma[]" value="Otros"></br>
		</div>
		<div>
			<br>
      	<td>Estado civil </td>
      	<td><select name="estado" id="estado" class="estado">
        	<option value="Soltero">Soltero</option>
        	<option value="Casado">Casado</option>
        	<option value="Divorciado">Divorciado</option>
        	<option value="Viudo">Viudo</option>
					<option value="Otros">Otros</option>
      	</select>
				</td>
			</br>
    </div>
    <div>
			<br><td>Sexo:</td>
			<td>
				<br>
       	Hombre <input name="sexo" type="radio" value="Hombre">
        Mujer <input name="sexo" type="radio" value="Mujer">
        Chino/a<input name="sexo" type="radio" value="Chino/a"></td>
			</br>
    </br>
	</div>

		<input type="hidden" name="grabar" value="si" />

		<!--<div><label>Mensaje:</label><textarea rows='6' name="mensaje" class="mensaje"><?php @$_POST['mensaje'] ?></textarea><?php echo @$error[5] ?></div>
-->
		<div class="demo"><input type='submit'  name="boton" class="boton" value='Envia Mensaje'></div>
	</form>
	</div>
</body>
</html>
